﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacles : MonoBehaviour {
    public GameObject[] ob;
    public Transform campos;

	// Use this for initialization
	void Start () {
        ObstacleMaker();
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.left * PlayerPrefs.GetInt("speed") * Time.deltaTime);
		
	}
    void ObstacleMaker()
    {
        GameObject clone = (GameObject)Instantiate(ob[Random.Range(0, ob.Length)], transform.position, Quaternion.identity);
        clone.name = "Quad";
        clone.transform.Translate(Vector3.down * 20 * Time.deltaTime);
        clone.AddComponent<BoxCollider2D>();
        clone.GetComponent<BoxCollider2D>().isTrigger = true;
        float xx = Random.Range(1, 4);
        Invoke("ObstacleMaker", xx);
    }  

    
}
